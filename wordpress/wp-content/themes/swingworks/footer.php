<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->

<footer id="footer-info">
    <div class="row">
        <div class="social">
            <div class="icons">
                <ul>
                    <li>
                        <a class="facebook" href="https://www.facebook.com/Sebastian-Platschek-Golftraining-1785155378376383/" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 1792 1792" fill="#dce6db">
                                <path d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z" />
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a class="instagram" href="https://www.instagram.com/swingworks.de/" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 1792 1792" fill="#dce6db">
                                <path d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm138 0q0 164-115 279t-279 115-279-115-115-279 115-279 279-115 279 115 115 279zm108-410q0 38-27 65t-65 27-65-27-27-65 27-65 65-27 65 27 27 65zm-502-220q-7 0-76.5-.5t-105.5 0-96.5 3-103 10-71.5 18.5q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103-3 96.5 0 105.5.5 76.5-.5 76.5 0 105.5 3 96.5 10 103 18.5 71.5q20 50 58 88t88 58q29 11 71.5 18.5t103 10 96.5 3 105.5 0 76.5-.5 76.5.5 105.5 0 96.5-3 103-10 71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103 3-96.5 0-105.5-.5-76.5.5-76.5 0-105.5-3-96.5-10-103-18.5-71.5q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10-96.5-3-105.5 0-76.5.5zm768 630q0 229-5 317-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124t-124-322q-5-88-5-317t5-317q10-208 124-322t322-124q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z" />
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a class="linkedin" href="https://de.linkedin.com/in/sebastian-platschek-219a02b1" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 1792 1792" fill="#dce6db">
                                <path d="M477 625v991h-330v-991h330zm21-306q1 73-50.5 122t-135.5 49h-2q-82 0-132-49t-50-122q0-74 51.5-122.5t134.5-48.5 133 48.5 51 122.5zm1166 729v568h-329v-530q0-105-40.5-164.5t-126.5-59.5q-63 0-105.5 34.5t-63.5 85.5q-11 30-11 81v553h-329q2-399 2-647t-1-296l-1-48h329v144h-2q20-32 41-56t56.5-52 87-43.5 114.5-15.5q171 0 275 113.5t104 332.5z" />
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div>
                <ul>
                    <li><a href="mailto:sp@swingworks.de?subject=Anfrage&body=Hallo%20Sebastian%2C">Kontakt</a></li>
                    <li><a href="/impressum">Datenschutz</a></li>
                    <li><a href="/impressum">Impressum</a></li>
                </ul>
            </div>
        </div>
        <div class="contact">
            <div class="empty">
            </div>
            <div class="logo">
                <a href="http://www.swingworks.de/" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.webp" alt="logo" loading="lazy">
                </a>
            </div>
            <div class="phone">
                <ul>
                    <li>
                        <p>Sebastian PLATSCHEK</p>
                    </li>
                    <li><a href="tel:+4991815206003">T. +49 9181 520 600 3</a></li>
                    <li><a href="tel:+4915124144294">M. +49 151 241 44 294</a></li>
                    <li><a href="mailto:sp@swingworks.de?subject=Anfrage&body=Hallo%20Sebastian%2C">sp@swingworks.de</a></li>
                </ul>
            </div>
            <div class="address">
                <ul>
                    <li>
                        <p>SwingWorks Systems GmbH</p>
                    </li>
                    <li>
                        <p>Maienbreite 59</p>
                    </li>
                    <li>
                        <p>92318 Neumarkt</p>
                    </li>
                    <li><a href="http://www.swingworks.de/" target="_blank">www.swingworks.de</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<footer id="footer-copyright">
    <div class="row">
        <div class="copyright">
            <span>© Copyright <?php echo date('Y'); ?></span>
            <span>|</span>
            <span>Sebastian PLATSCHEK</span>
        </div>
        <div class="scroll-top">
            <a href="#site-header" class="arrow"></a>
        </div>
    </div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

<script>
    const viewports = document.getElementsByClassName('golfclub-image');
    const isTouchDevice = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
    for (var i = 0; i < viewports.length; i++) {
        let viewport = viewports[i];
        let content = viewport.querySelector('.panorama');
        let image = content.querySelector('img');
        let sb = new ScrollBooster({
            viewport,
            content,
            scrollMode: 'native',
            direction: 'horizontal',
            pointerDownPreventDefault: !isTouchDevice
        });
        image.addEventListener('load', () => {
            const offsetX = image.scrollWidth - viewport.offsetWidth;
            const offsetY = image.scrollHeight - viewport.offsetHeight;
            sb.setPosition({
                x: offsetX / 2,
                y: offsetY / 2
            });
        });
    }
</script>

</body>

</html>