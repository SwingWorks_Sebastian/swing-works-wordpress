<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if (!function_exists('chld_thm_cfg_locale_css')) :
    function chld_thm_cfg_locale_css($uri)
    {
        if (empty($uri) && is_rtl() && file_exists(get_template_directory() . '/rtl.css'))
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter('locale_stylesheet_uri', 'chld_thm_cfg_locale_css');

if (!function_exists('child_theme_configurator_asset')) :
    function child_theme_configurator_asset()
    {
        wp_enqueue_style('chld_thm_cfg_child', trailingslashit(get_stylesheet_directory_uri()) . 'style.css', array('twenty-twenty-one-style', 'twenty-twenty-one-style', 'twenty-twenty-one-print-style'));
        wp_enqueue_script('chld_thm_cfg_dragtoscroll', trailingslashit(get_stylesheet_directory_uri()) . 'assets/js/scrollbooster.min.js', '', '', true);
    }
endif;
add_action('wp_enqueue_scripts', 'child_theme_configurator_asset', 10);

// END ENQUEUE PARENT ACTION

if (!function_exists('swing_works_setup')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Twenty Twenty-One 1.0
     *
     * @return void
     */
    function swing_works_setup()
    {
        // Add custom editor font sizes.
        add_theme_support(
            'editor-font-sizes',
            array(
                array(
                    'name'      => esc_html__('17', 'swingworks'),
                    'shortName' => esc_html_x('17', 'Font size', 'swingworks'),
                    'size'      => 17,
                    'slug'      => '17',
                ),
                array(
                    'name'      => esc_html__('18', 'swingworks'),
                    'shortName' => esc_html_x('18', 'Font size', 'swingworks'),
                    'size'      => 18,
                    'slug'      => '18',
                ),
                array(
                    'name'      => esc_html__('20', 'swingworks'),
                    'shortName' => esc_html_x('20', 'Font size', 'swingworks'),
                    'size'      => 20,
                    'slug'      => '20',
                ),
                array(
                    'name'      => esc_html__('22', 'swingworks'),
                    'shortName' => esc_html_x('22', 'Font size', 'swingworks'),
                    'size'      => 22,
                    'slug'      => '22',
                ),
                array(
                    'name'      => esc_html__('24', 'swingworks'),
                    'shortName' => esc_html_x('24', 'Font size', 'swingworks'),
                    'size'      => 24,
                    'slug'      => '24',
                ),
                array(
                    'name'      => esc_html__('28', 'swingworks'),
                    'shortName' => esc_html_x('28', 'Font size', 'swingworks'),
                    'size'      => 28,
                    'slug'      => '28',
                ),
                array(
                    'name'      => esc_html__('36', 'swingworks'),
                    'shortName' => esc_html_x('36', 'Font size', 'swingworks'),
                    'size'      => 36,
                    'slug'      => '36',
                ),
                array(
                    'name'      => esc_html__('48', 'swingworks'),
                    'shortName' => esc_html_x('48', 'Font size', 'swingworks'),
                    'size'      => 48,
                    'slug'      => '48',
                ),
                array(
                    'name'      => esc_html__('52', 'swingworks'),
                    'shortName' => esc_html_x('52', 'Font size', 'swingworks'),
                    'size'      => 52,
                    'slug'      => '52',
                ),
            )
        );

        /**
         * Register Block Pattern Category.
         */
        if (function_exists('register_block_pattern_category')) {

            register_block_pattern_category(
                'swingworks',
                array('label' => esc_html__('Swing Works', 'swingworks'))
            );
        }

        /**
         * Register Block Patterns.
         */
        if (function_exists('register_block_pattern')) {

            register_block_pattern(
                'swingworks/intro',
                array(
                    'title'       => esc_html__('Intro', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="intro"><div class="wp-block-group__inner-container"><!-- wp:columns {"align":"full"} --><div class="wp-block-columns alignfull"><!-- wp:column {"width":"","className":"intro-text-col"} --><div class="wp-block-column intro-text-col"><!-- wp:group {"className":"intro-text-group"} --><div class="wp-block-group intro-text-group"><div class="wp-block-group__inner-container"><!-- wp:heading {"level":1,"textColor":"dark-gray","fontSize":"52"} --><h1 class="has-dark-gray-color has-text-color has-52-font-size">Sebastian<br>PLATSCHEK</h1><!-- /wp:heading --><!-- wp:heading {"textColor":"green","className":"special","fontSize":"22"} --><h2 class="special has-green-color has-text-color has-22-font-size">Class A — Fully Qualified<br>Golfteacher &amp; Golfprofessional<br>PGA of Germany</h2><!-- /wp:heading --><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"20"} --><p class="has-dark-gray-color has-text-color has-20-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod <a href="#">tempor invidunt</a> ut labore et dolore magna erat, sed diam voluptua.</p><!-- /wp:paragraph --><!-- wp:list {"fontSize":"18"} --><ul class="has-18-font-size"><li><strong><a href="#golfpro" data-type="internal" data-id="#golfpro">GOLF-PROFESSIONAL PGA</a></strong></li><li><strong><a href="#golfpro-reverse" data-type="internal" data-id="#golfpro-reverse">GOLFSCHUL-MANAGEMENT</a></strong></li></ul><!-- /wp:list --></div></div><!-- /wp:group --></div><!-- /wp:column --><!-- wp:column {"width":"","className":"intro-image-col"} --><div class="wp-block-column intro-image-col"></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group -->',
                )
            );

            register_block_pattern(
                'swingworks/golfpro',
                array(
                    'title'       => esc_html__('Golf Pro', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="golfpro"><div class="wp-block-group__inner-container"><!-- wp:columns {"align":"full"} --><div class="wp-block-columns alignfull"><!-- wp:column {"className":"golfpro-image-col"} --><div class="wp-block-column golfpro-image-col"></div><!-- /wp:column --><!-- wp:column {"className":"golfpro-text-col"} --><div class="wp-block-column golfpro-text-col"><!-- wp:group {"className":"golfpro-text-group"} --><div class="wp-block-group golfpro-text-group"><div class="wp-block-group__inner-container"><!-- wp:heading {"level":1,"textColor":"dark-gray","fontSize":"52"} --><h1 class="has-dark-gray-color has-text-color has-52-font-size">Sebastian Platschek –<br>Golfprofessional, PGA</h1><!-- /wp:heading --><!-- wp:heading {"fontSize":"22"} --><h2 class="has-22-font-size">Golf als Leidenschaft – Unterrichten als Berufung</h2><!-- /wp:heading --><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size">Seit mehr als 15 Jahren coache ich Spieler jeglicher Spielstärke. Neben den technischen Aspekten (unterstützt durch modernste Hilfsmittel (Trackman 4)), lege ich großen Wert auf eine vernünftige Planung in der Herangehensweise an die unzähligen Aufgaben, die uns der Golfplatz täglich stellt.</p><!-- /wp:paragraph --><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size">Nur wer seine Skills kennt, diese ständig erweitert und dadurch ein breites Spektrum smarter Lösungswege kennt, wird nachhaltig Erfolge erzielen.</p><!-- /wp:paragraph --><!-- wp:list {"textColor":"dark-gray"} --><ul class="has-dark-gray-color has-text-color"><li><strong>Fully Qualified PGA Golfprofessionsl | PGA of Germany</strong></li><li><strong>A – Trainer DGV | DOSB</strong></li><li><strong>Trackman Certified Instructor (Level 2)</strong></li></ul><!-- /wp:list --><!-- wp:buttons --><div class="wp-block-buttons"><!-- wp:button --><div class="wp-block-button"><a class="wp-block-button__link" href="mailto:sp@swingworks.de?subject=Anfrage&amp;body=Hallo%20Sebastian%2C"><strong>KONTAKT AUFNEHMEN</strong></a></div><!-- /wp:button --></div><!-- /wp:buttons --></div></div><!-- /wp:group --></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group -->',
                )
            );

            register_block_pattern(
                'swingworks/golfpro-reverse',
                array(
                    'title'       => esc_html__('Golf Pro Reverse', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="golfpro-reverse"><div class="wp-block-group__inner-container"><!-- wp:columns {"align":"full"} --><div class="wp-block-columns alignfull"><!-- wp:column {"className":"golfpro-reverse-text-col"} --><div class="wp-block-column golfpro-reverse-text-col"><!-- wp:group {"className":"golfpro-reverse-text-group"} --><div class="wp-block-group golfpro-reverse-text-group"><div class="wp-block-group__inner-container"><!-- wp:heading {"level":1,"textColor":"dark-gray","fontSize":"52"} --><h1 class="has-dark-gray-color has-text-color has-52-font-size">Sebastian Platschek –<br>Golfschul-Management</h1><!-- /wp:heading --><!-- wp:heading {"fontSize":"22"} --><h2 class="has-22-font-size">Konzeptionell. Analytisch. Lösungsorientiert.</h2><!-- /wp:heading --><!-- wp:list {"textColor":"dark-gray"} --><ul class="has-dark-gray-color has-text-color"><li><strong>Mitgründer und Geschäftsführer der SwingWorks Systems GmbH</strong></li><li><strong>Leiter von drei erfolgreichen Golfschulen</strong></li><li><strong>Beratung von Golfschulen und Golfclubs</strong></li><li><strong>Erstellung und Umsetzung von Konzepten zur Kundengewinnung und Bindung</strong></li></ul><!-- /wp:list --><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size">Durch meine Arbeit bei SwingWorks und den Kontakt mit vielen Clubmanagern, Vorständen und Geschäftsführern kenne ich die Sorgen und Nöte beider Seiten (Golfclub und Golfschule).</p><!-- /wp:paragraph --><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size">Eine Begegnung auf Augenhöhe und das gezielte Umsetzen erfolgsgetesteter Konzepte bilden die Basis für nachhaltigen Erfolg.</p><!-- /wp:paragraph --><!-- wp:buttons --><div class="wp-block-buttons"><!-- wp:button --><div class="wp-block-button"><a class="wp-block-button__link" href="mailto:sp@swingworks.de?subject=Anfrage&amp;body=Hallo%20Sebastian%2C">Kontakt aufnehmen</a></div><!-- /wp:button --></div><!-- /wp:buttons --></div></div><!-- /wp:group --></div><!-- /wp:column --><!-- wp:column {"className":"golfpro-reverse-image-col"} --><div class="wp-block-column golfpro-reverse-image-col"></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group -->',
                )
            );

            register_block_pattern(
                'swingworks/golfplatze',
                array(
                    'title'       => esc_html__('Golf Platze', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group --><div id="golfplatze" class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:columns --><div class="wp-block-columns"><!-- wp:column {"className":"golfplatze-intro"} --><div class="wp-block-column golfplatze-intro"><!-- wp:heading {"level":1,"textColor":"dark-gray"} --><h1 class="has-dark-gray-color has-text-color">Unsere Golfplätze!</h1><!-- /wp:heading --><!-- wp:paragraph {"textColor":"dark-gray"} --><p class="has-dark-gray-color has-text-color">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p><!-- /wp:paragraph --></div><!-- /wp:column --><!-- wp:column {"className":"golfplatze-nav"} --><div class="wp-block-column golfplatze-nav"><!-- wp:list {"fontSize":"18"} --><ul class="has-18-font-size"><li><strong>Erlangen</strong></li><li><strong>Herrnhoff</strong></li><li><strong>Steigerwald</strong></li></ul><!-- /wp:list --></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group -->',
                )
            );

            register_block_pattern(
                'swingworks/golfclub-erlangen',
                array(
                    'title'       => esc_html__('Golf Club Erlangen', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="erlangen"><div class="wp-block-group__inner-container"><!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="golfclub"><div class="wp-block-group__inner-container"><!-- wp:columns --><div class="wp-block-columns"><!-- wp:column {"className":"golfclub-text"} --><div class="wp-block-column golfclub-text"><!-- wp:heading {"level":1} --><h1>Golf-Club Erlangen</h1><!-- /wp:heading --><!-- wp:paragraph {"textColor":"dark-gray"} --><p class="has-dark-gray-color has-text-color">Eingebettet in die hügelige Landschaft der Fränkischen Schweiz, bietet unser 18-Loch Platz sportlich anspruchsvolle Bahnen. Mit unserer professionellen Driving Range und einem großzügigen Übungsbereich sind Ihnen auch im Bereich Training keine Grenzen gesetzt.</p><!-- /wp:paragraph --><!-- wp:group --><div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size"><strong><span class="has-inline-color has-green-color">Gründung</span> 2011 / <span class="has-inline-color has-green-color">Leitung</span> Sebastian Platschek / <span class="has-inline-color has-green-color">Head-Pro</span> Sebastian Platschek / <span class="has-inline-color has-green-color">Pros</span> Max Zeitler, Blain Bertrand, René Wittmann</strong></p><!-- /wp:paragraph --><!-- wp:paragraph {"className":"button-arrow-right","fontSize":"18"} --><p class="button-arrow-right has-18-font-size"><strong><a href="https://golf.swingworks.de/de/golfclubs/golf-club-erlangen" data-type="URL" data-id="https://golf.swingworks.de/de/golfclubs/golf-club-erlangen" target="_blank" rel="noreferrer noopener">INFOS</a></strong><a href="#"> </a><span>→</span></p><!-- /wp:paragraph --></div></div><!-- /wp:group --></div><!-- /wp:column --><!-- wp:column {"className":"golfclub-image"} --><div class="wp-block-column golfclub-image"><!-- wp:image {"id":182,"sizeSlug":"large","linkDestination":"none"} --><figure class="wp-block-image size-large"><img src="http://localhost:8080/wp-content/uploads/2021/06/Ebene-89.png" alt="" class="wp-image-182"/></figure><!-- /wp:image --></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group --></div></div><!-- /wp:group -->',
                )
            );

            register_block_pattern(
                'swingworks/golfclub-herrnhof',
                array(
                    'title'       => esc_html__('Golf Club Herrnhof', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="herrnhof"><div class="wp-block-group__inner-container"><!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="golfclub"><div class="wp-block-group__inner-container"><!-- wp:columns --><div class="wp-block-columns"><!-- wp:column {"className":"golfclub-image"} --><div class="wp-block-column golfclub-image"><!-- wp:image {"id":185,"sizeSlug":"large","linkDestination":"none"} --><figure class="wp-block-image size-large"><img src="http://localhost:8080/wp-content/uploads/2021/06/Ebene-117-1024x302.png" alt="" class="wp-image-185"/></figure><!-- /wp:image --></div><!-- /wp:column --><!-- wp:column {"className":"golfclub-text"} --><div class="wp-block-column golfclub-text"><!-- wp:heading {"level":1} --><h1>Golf-Club Herrnhof</h1><!-- /wp:heading --><!-- wp:paragraph {"textColor":"dark-gray"} --><p class="has-dark-gray-color has-text-color">Der 18-Loch-Golfplatz ist in eine leicht hügelige Landschaft unterhalb des Grünbergs eingebettet. Er verfügt über eine schnelle und verkehrsgünstige Anbindung an den Raum Nürnberg-Fürth durch die nah gelegene Bundesstrasse B 8 sowie über die Autobahnen A 3 und A 9.</p><!-- /wp:paragraph --><!-- wp:group --><div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size"><strong><span class="has-inline-color has-green-color">Gründung</span> 2008 / <span class="has-inline-color has-green-color">Leitung</span> Sebastian Platschek / <span class="has-inline-color has-green-color">Head-Pro</span> Sebastian Platschek / <span class="has-inline-color has-green-color">Pros</span> Fabio Irrgang, Blain Bertrand</strong></p><!-- /wp:paragraph --><!-- wp:paragraph {"className":"button-arrow-right","fontSize":"18"} --><p class="button-arrow-right has-18-font-size"><a href="https://golf.swingworks.de/de/golfclubs/golfclub-herrnhof-e-v" data-type="URL" data-id="https://golf.swingworks.de/de/golfclubs/golfclub-herrnhof-e-v" target="_blank" rel="noreferrer noopener"><strong>INFOS</strong> </a><span>→</span></p><!-- /wp:paragraph --></div></div><!-- /wp:group --></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group --></div></div><!-- /wp:group -->',
                )
            );

            register_block_pattern(
                'swingworks/golfclub-steigerwald',
                array(
                    'title'       => esc_html__('Golf Club Steigerwald', 'textdomain'),
                    'categories'  => array('swingworks'),
                    'content'     => '<!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="steigerwald"><div class="wp-block-group__inner-container"><!-- wp:group {"align":"wide"} --><div class="wp-block-group alignwide" id="golfclub"><div class="wp-block-group__inner-container"><!-- wp:columns --><div class="wp-block-columns"><!-- wp:column {"className":"golfclub-text"} --><div class="wp-block-column golfclub-text"><!-- wp:heading {"level":1} --><h1>Golf-Club Steigerwald</h1><!-- /wp:heading --><!-- wp:paragraph {"textColor":"dark-gray"} --><p class="has-dark-gray-color has-text-color">Direkt an der Nord-Süd Achse der Autobahn A3, liegt der Golfclub Steigerwald in Geiselwind. Ein Golfplatz, der sowohl Herausforderung als auch Idyll zum Entspannen ist. Der 18-Loch-Meisterschaftsplatz wurde von Stararchitekt Don Harradine vor über 25 Jahren designt.</p><!-- /wp:paragraph --><!-- wp:group --><div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:paragraph {"textColor":"dark-gray","fontSize":"18"} --><p class="has-dark-gray-color has-text-color has-18-font-size"><strong><span class="has-inline-color has-green-color">Gründung</span> 2020 / <span class="has-inline-color has-green-color">Leitung</span> Sebastian Platschek / <span class="has-inline-color has-green-color">Head-Pro</span> Max Zeitler / <span class="has-inline-color has-green-color">Pros</span> Horst Rosenkranz</strong></p><!-- /wp:paragraph --><!-- wp:paragraph {"className":"button-arrow-right","fontSize":"18"} --><p class="button-arrow-right has-18-font-size"><a href="https://golf.swingworks.de/de/golfclubs/golfclub-steigerwald-in-geiselwind-e-v" data-type="URL" data-id="https://golf.swingworks.de/de/golfclubs/golfclub-steigerwald-in-geiselwind-e-v" target="_blank" rel="noreferrer noopener"><strong>INFOS</strong> </a><span>→</span></p><!-- /wp:paragraph --></div></div><!-- /wp:group --></div><!-- /wp:column --><!-- wp:column {"className":"golfclub-image"} --><div class="wp-block-column golfclub-image"><!-- wp:image {"id":186,"sizeSlug":"large","linkDestination":"none"} --><figure class="wp-block-image size-large"><img src="http://localhost:8080/wp-content/uploads/2021/06/Ebene-119.png" alt="" class="wp-image-186"/></figure><!-- /wp:image --></div><!-- /wp:column --></div><!-- /wp:columns --></div></div><!-- /wp:group --></div></div><!-- /wp:group -->',
                )
            );
        }

        // Enqueue editor styles.
        add_editor_style('style.css');

        // Custom background color.
        add_theme_support(
            'custom-background',
            array(
                'default-color' => 'd1e4dd',
            )
        );

        // Editor color palette.
        $black     = '#000000';
        $dark_gray = '#32504c';
        $light_gray = '#f2f0f0';
        $gray      = '#dce6db';
        $green     = '#0c7554';
        $blue      = '#D1DFE4';
        $purple    = '#D1D1E4';
        $red       = '#E4D1D1';
        $orange    = '#E4DAD1';
        $yellow    = '#dee410';
        $white     = '#FFFFFF';

        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name'  => esc_html__('Black', 'swingworks'),
                    'slug'  => 'black',
                    'color' => $black,
                ),
                array(
                    'name'  => esc_html__('Dark gray', 'swingworks'),
                    'slug'  => 'dark-gray',
                    'color' => $dark_gray,
                ),
                array(
                    'name'  => esc_html__('Light gray', 'swingworks'),
                    'slug'  => 'light-gray',
                    'color' => $light_gray,
                ),
                array(
                    'name'  => esc_html__('Gray', 'swingworks'),
                    'slug'  => 'gray',
                    'color' => $gray,
                ),
                array(
                    'name'  => esc_html__('Green', 'swingworks'),
                    'slug'  => 'green',
                    'color' => $green,
                ),
                array(
                    'name'  => esc_html__('Blue', 'swingworks'),
                    'slug'  => 'blue',
                    'color' => $blue,
                ),
                array(
                    'name'  => esc_html__('Purple', 'swingworks'),
                    'slug'  => 'purple',
                    'color' => $purple,
                ),
                array(
                    'name'  => esc_html__('Red', 'swingworks'),
                    'slug'  => 'red',
                    'color' => $red,
                ),
                array(
                    'name'  => esc_html__('Orange', 'swingworks'),
                    'slug'  => 'orange',
                    'color' => $orange,
                ),
                array(
                    'name'  => esc_html__('Yellow', 'swingworks'),
                    'slug'  => 'yellow',
                    'color' => $yellow,
                ),
                array(
                    'name'  => esc_html__('White', 'swingworks'),
                    'slug'  => 'white',
                    'color' => $white,
                ),
            )
        );
    }
}

add_action('init', 'swing_works_setup');

/**
 * Changes the locale output.
 * 
 * @param string $locale The current locale.
 * 
 * @return string The locale.
 */
function yst_wpseo_change_og_locale($locale)
{
    return 'de_de';
}

add_filter('wpseo_locale', 'yst_wpseo_change_og_locale');