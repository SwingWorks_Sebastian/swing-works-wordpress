<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://wordpress.org/support/article/editing-wp-config-php/

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', 'swingworks' );


/** MySQL database username */

define( 'DB_USER', 'root' );


/** MySQL database password */

define( 'DB_PASSWORD', 'SummerIsComing.' );


/** MySQL hostname */

define( 'DB_HOST', 'mariadb:3306' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         '~GaC!xH,=DUPe36BZOGJIxL{^[5q9-M7r)z5A7QH23*i{=EJ9|90:fr]=-+vVHuF' );

define( 'SECURE_AUTH_KEY',  '7 4]LM.Ppvb4v}u**Pvy~y gu/,j&~4k4<iO ulyokps%Ls&Z>wSZ_wzWvnJNWyW' );

define( 'LOGGED_IN_KEY',    'm&Y&_{81Hfz $CgeRew-cM1z]1tbIt65OkZ VN]zHpLhfW[}uxz$Oqo{Bz?5@Mz0' );

define( 'NONCE_KEY',        '$8xYnw55S_z~7ql)TR&~hjs;LTR5*/e;`Nsr]pf_]_qc$RLz;T2#>?}aKR~AsZHs' );

define( 'AUTH_SALT',        '(rr*,Ex4OXF#P^L]F-)IUuo[}MDiw*i-Q#@(w+`JXQ_-oMZ_pl$I&bVoELN-_NES' );

define( 'SECURE_AUTH_SALT', 'c0fb33cTIOM[20o8erWOJ%yNrQ13Xh)LZyF$)}1LU6V)zS<CxGYAaZ-!wzr&5DAb' );

define( 'LOGGED_IN_SALT',   '6xw2nN~dEhUl]?wtu(Oh.}aoMzaA(Fc`YiQQw(D~9onf@mMe/5nE6M:eJ,q3|+qo' );

define( 'NONCE_SALT',       'OTTN8U-sFD8E*cRUw%a7&;;y:;VO49To=$>8@/TUF;#$<Cu.y51%3}l+eao4H%zj' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the documentation.

 *

 * @link https://wordpress.org/support/article/debugging-in-wordpress/

 */

define( 'WP_DEBUG', false );


define( 'FS_METHOD', 'direct' );
/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','http://example.com');
 *  define('WP_SITEURL','http://example.com');
 *
 */
if ( defined( 'WP_CLI' ) ) {
	$_SERVER['HTTP_HOST'] = '127.0.0.1';
}

define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/' );
define( 'WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/' );
define( 'WP_AUTO_UPDATE_CORE', false );
/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', __DIR__ . '/' );

}


/** Sets up WordPress vars and included files. */

require_once ABSPATH . 'wp-settings.php';

/**
 * Disable pingback.ping xmlrpc method to prevent WordPress from participating in DDoS attacks
 * More info at: https://docs.bitnami.com/general/apps/wordpress/troubleshooting/xmlrpc-and-pingback/
 */
if ( !defined( 'WP_CLI' ) ) {
	// remove x-pingback HTTP header
	add_filter("wp_headers", function($headers) {
		unset($headers["X-Pingback"]);
		return $headers;
	});
	// disable pingbacks
	add_filter( "xmlrpc_methods", function( $methods ) {
		unset( $methods["pingback.ping"] );
		return $methods;
	});
}
