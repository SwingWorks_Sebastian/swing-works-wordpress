# Swing Works

Sebastian Platschek ist Fully Qualified Golf Professional von der PGA of Germany und führt drei erfolgreiche Golfschulen.

## Installation

> NOTE: As this is a non-root container, the mounted files and directories must have proper permissions.

```sh
sudo sh fix-wordpress-permissions.sh ./wordpress/
```

####

Open your favorite Terminal and run the below commands to set up for the application.

```sh
$ docker-compose build
$ docker-compose up -d
```

Verify the deployment by navigating to your server address in your preferred browser.

```sh
http://localhost/
```

## Initiation

1. **Active plugins**  
Download and active those plugins: Autoptimize, Blocks Animation, SmushIt, Smart Slider 3, Wordpress Importer, Yoast SEO.
2. **Upload Media**  
Go to Media Library and upload all image from `/backup/image/` folder.
3. **Import Slider**  
Go to Smart Slider, choose New Project and select option Or Import Your Own Files to import each slider from `/backup/smart slider/` folder.
4. **Import Page**  
Go to Tools -> Import, select Run Importer at the end of the list and import the latest page from `/backup/page/` folder.
Then edit the Page name Homepage and reselect the corresponding slider for each section (order from Intro -> Golf Pro -> Golf Pro Reserve -> Showcase).


## License

[MIT](https://choosealicense.com/licenses/mit/)
